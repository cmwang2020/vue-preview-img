# vue-preview-cm

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

## 安装方法


## params
```bash
title: '' // 传入插件的显示左上角的名称 
lstImg: [] // 存入图片路径
showEnlargeBool: false, // 是否显示右下角放大功能
autoplay: false, // 是否自动播放
autoplay: false, // 是否自动播放
show () // 调用的方法

```
## example
```bash
 let params = {
  title: '图片标题',
  lstImg: ['图片路径']
}
this.$refs.previewImg.show(params) 
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
