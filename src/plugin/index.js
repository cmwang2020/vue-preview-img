import previewImg from './previewImg.vue'

const plugin = {
  install: function(Vue, opt){
    Vue.component(previewImg.name, previewImg)
  }
}

export default plugin